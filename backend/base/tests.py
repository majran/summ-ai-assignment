from django.test import TestCase

# Create your tests here.

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from rest_framework import status
from .models import Translation

from .views import TranslationViewSet


class TranslationViewSetTestCase(TestCase):
    def setUp(self):
        self.translation = Translation.objects.create(
            from_user="testuser", input_text="Hi", output_text="Hola"
        )
        self.factory = APIRequestFactory()
        self.user = User.objects.create_user(
            username="testuser", email="testuser@example.com", password="testpass"
        )

    def test_retrieve_translation(self):
        response = self.client.get(
            reverse("translation-detail", args=[self.translation.id])
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["input_text"], self.translation.input_text)

    def test_update_translation(self):
        new_input_text = "Bonjour le monde!"
        data = {"input_text": new_input_text}
        response = self.client.put(
            reverse("translation-detail", args=[self.translation.id]),
            data=data,
            content_type="application/json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.translation.refresh_from_db()
        self.assertEqual(self.translation.input_text, new_input_text)

    def test_delete_translation(self):
        response = self.client.delete(
            reverse("translation-detail", args=[self.translation.id])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Translation.objects.filter(id=self.translation.id).exists())

    def test_perform_create(self):
        self.assertEqual(Translation.objects.count(), 1)
        view = TranslationViewSet.as_view({"post": "create"})
        data = {"input_text": "Hello", "output_text": "Bonjour"}
        request = self.factory.post(reverse("translation-list"), data=data)
        request.user = self.user
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Translation.objects.count(), 2)
        self.assertEqual(Translation.objects.last().from_user, self.user.username)
