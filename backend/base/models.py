import uuid

from django.db import models


class Translation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    input_text = models.CharField(max_length=256)
    output_text = models.CharField(max_length=256)
    from_user = models.CharField(max_length=256)
