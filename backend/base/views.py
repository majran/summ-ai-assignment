from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from base.serializers import (
    TranslationListSerializer,
    TranslationCreateSerializer,
    TranslationUpdateSerializer,
)
from base.models import Translation


class TranslationViewSet(ModelViewSet):
    serializer_class = TranslationListSerializer
    # permission_classes = (IsAuthenticated,)
    http_method_names = ["get", "post", "put", "delete"]
    queryset = Translation.objects.all()

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return self.serializer_class
        elif self.action == "update":
            return TranslationUpdateSerializer
        elif self.action == "create":
            return TranslationCreateSerializer
        else:
            return self.serializer_class

    def perform_create(self, serializer):
        serializer.save(from_user=self.request.user)
