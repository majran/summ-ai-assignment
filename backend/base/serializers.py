from rest_framework import serializers

from base.models import Translation


class TranslationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Translation
        fields = "__all__"


class TranslationCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Translation
        fields = ["input_text", "output_text"]


class TranslationUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Translation
        fields = ["input_text", "output_text"]
        extra_kwargs = {
            "input_text": {"required": False},
            "output_text": {
                "required": False,
            },
        }
