from rest_framework.routers import DefaultRouter

from base.views import TranslationViewSet


urlpatterns = []

router = DefaultRouter()
router.register("translation", TranslationViewSet, basename="translation")

urlpatterns += router.urls
