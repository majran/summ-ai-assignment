# Run code on a machine with docker and docker-compose 

1. build image (**Remember to build new image everytime you update the code**)

```bash
docker-compose build
```

2. run

```bash
docker-compose up
```

Now you can check frontend from `http://localhost/3000` and backend api from `http://localhost:8000/v1/api/`. 
You also can add a new super user by running `docker-compose run backend python manage.py createsuperuser`. Then login via `http://localhost:8000/admin/` and then create new `translations` from `http://localhost:8000/v1/api/` and check the result in `http://localhost/3000`


# Test codes
Run the following command on a machine with docker and docker-compose

```bash
docker-compose run backend ./entry.sh test
```