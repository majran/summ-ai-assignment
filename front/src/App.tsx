import React from 'react';

import Translation from 'pages/Transaction';

import './App.css';

function App() {
  return (
    <div className="App">
      <Translation />
    </div>
  );
}

export default App;
