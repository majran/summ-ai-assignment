export type Translation = {
  id: number;
  input_text: string;
  output_text: string;
  from_user: string;
};
