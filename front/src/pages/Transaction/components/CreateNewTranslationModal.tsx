import React, { useState } from 'react';

import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
  TextField,
} from '@mui/material';

import { MRT_ColumnDef } from 'material-react-table';

import { Translation as TranslationType } from 'types';

import { useAddTranslationMutation } from 'services/translation';

interface CreateModalProps {
  columns: MRT_ColumnDef<TranslationType>[];
  onClose: () => void;
  open: boolean;
}

const CreateNewTranslationModal = ({
  open,
  columns,
  onClose,
}: CreateModalProps) => {
  const [values, setValues] = useState<any>(() =>
    columns.reduce((acc, column) => {
      acc[column.accessorKey ?? ''] = '';
      return acc;
    }, {} as any)
  );

  const [addTranslation] = useAddTranslationMutation();

  const handleCreateNewRow = (values: Partial<TranslationType>) => {
    addTranslation(values);
  };

  const handleSubmit = () => {
    //put your validation logic here
    handleCreateNewRow(values);
    onClose();
  };

  return (
    <Dialog open={open}>
      <DialogTitle textAlign="center">Create New Translation</DialogTitle>
      <DialogContent>
        <form onSubmit={(e) => e.preventDefault()}>
          <Stack
            sx={{
              width: '100%',
              minWidth: { xs: '300px', sm: '360px', md: '400px' },
              gap: '1.5rem',
            }}
          >
            {columns.map((column) => (
              <TextField
                key={column.accessorKey}
                label={column.header}
                name={column.accessorKey}
                onChange={(e) =>
                  setValues({ ...values, [e.target.name]: e.target.value })
                }
              />
            ))}
          </Stack>
        </form>
      </DialogContent>
      <DialogActions sx={{ p: '1.25rem' }}>
        <Button onClick={onClose}>Cancel</Button>
        <Button color="secondary" onClick={handleSubmit} variant="contained">
          Create New Translation
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CreateNewTranslationModal;
