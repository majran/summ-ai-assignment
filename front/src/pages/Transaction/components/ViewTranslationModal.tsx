import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Stack,
  TextField,
} from '@mui/material';

import { MRT_ColumnDef } from 'material-react-table';

import { Translation as TranslationType } from 'types';

import { useGetTranslationByIdQuery } from 'services/translation';

interface ViewModalProps {
  columns: MRT_ColumnDef<TranslationType>[];
  onClose: () => void;
  selectedTranslationId: string | null;
}

const ViewTranslationModal = ({
  selectedTranslationId,
  columns,
  onClose,
}: ViewModalProps) => {
  const { data } = useGetTranslationByIdQuery(selectedTranslationId || '');

  return (
    <Dialog open={selectedTranslationId != null}>
      <DialogTitle textAlign="center">View Translation</DialogTitle>
      <DialogContent>
        <Stack
          sx={{
            width: '100%',
            minWidth: { xs: '300px', sm: '360px', md: '400px' },
            gap: '1.5rem',
          }}
        >
          {columns.map((column) => (
            <TextField
              key={column.accessorKey}
              label={column.header}
              name={column.accessorKey}
              value={data ? data[column.accessorKey || 'id'] : ''}
              disabled
            />
          ))}
        </Stack>
      </DialogContent>
      <DialogActions sx={{ p: '1.25rem' }}>
        <Button onClick={onClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};

export default ViewTranslationModal;
