import React, { useCallback, useMemo, useState } from 'react';
import MaterialReactTable, {
  MaterialReactTableProps,
  MRT_Cell,
  MRT_ColumnDef,
  MRT_Row,
} from 'material-react-table';
import { Box, Button, IconButton, Tooltip } from '@mui/material';
import { Delete, Edit, Visibility } from '@mui/icons-material';

import {
  useGetTranslationListQuery,
  useUpdateTranslationMutation,
  useDeleteTranslationMutation,
} from 'services/translation';
import { Translation as TranslationType } from 'types';

import CreateNewTranslationModal from './components/CreateNewTranslationModal';
import ViewTranslationModal from './components/ViewTranslationModal';

const TranslationComponent = () => {
  const [createModalOpen, setCreateModalOpen] = useState(false);
  const [selectedTranslationId, setSelectedTranslationId] = useState(null);
  const { data: tableData, error, isLoading } = useGetTranslationListQuery();
  const [updateTranslation] = useUpdateTranslationMutation();
  const [deleteTranslation] = useDeleteTranslationMutation();

  const [validationErrors, setValidationErrors] = useState<{
    [cellId: string]: string;
  }>({});

  const handleSaveRowEdits: MaterialReactTableProps<TranslationType>['onEditingRowSave'] =
    async ({ exitEditingMode, row, values }) => {
      if (!Object.keys(validationErrors).length) {
        updateTranslation({
          id: row.getValue('id'),
          body: {
            input_text: values.input_text,
            output_text: values.output_text,
          },
        });
        exitEditingMode(); //required to exit editing mode and close modal
      }
    };

  const handleCancelRowEdits = () => {
    setValidationErrors({});
  };

  const handleDeleteRow = (row: MRT_Row<TranslationType>) => {
    if (
      !window.confirm(`Are you sure you want to delete ${row.getValue('id')}`)
    ) {
      return;
    }
    deleteTranslation(row.getValue('id'));
  };

  const getCommonEditTextFieldProps = useCallback(
    (
      cell: MRT_Cell<TranslationType>
    ): MRT_ColumnDef<TranslationType>['muiTableBodyCellEditTextFieldProps'] => {
      return {
        error: !!validationErrors[cell.id],
        helperText: validationErrors[cell.id],
        onBlur: (event) => {
          const isValid = validateRequired(event.target.value);
          if (!isValid) {
            //set validation error for cell if invalid
            setValidationErrors({
              ...validationErrors,
              [cell.id]: `${cell.column.columnDef.header} is required`,
            });
          } else {
            //remove validation error for cell if valid
            delete validationErrors[cell.id];
            setValidationErrors({
              ...validationErrors,
            });
          }
        },
      };
    },
    [validationErrors]
  );

  const columns = useMemo<MRT_ColumnDef<TranslationType>[]>(
    () => [
      {
        accessorKey: 'id',
        header: 'ID',
        enableColumnOrdering: false,
        enableEditing: false, //disable editing on this column
        enableSorting: false,
      },
      {
        accessorKey: 'input_text',
        header: 'Input Text',
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'output_text',
        header: 'Output Text',
        muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
          ...getCommonEditTextFieldProps(cell),
        }),
      },
      {
        accessorKey: 'from_user',
        header: 'From User',
        enableEditing: false,
      },
    ],
    [getCommonEditTextFieldProps]
  );

  if (error) {
    return <>Oh no, there was an error</>;
  }

  if (isLoading) {
    return <>Loading...</>;
  }
  return (
    <>
      <MaterialReactTable
        displayColumnDefOptions={{
          'mrt-row-actions': {
            muiTableHeadCellProps: {
              align: 'center',
            },
            size: 120,
          },
        }}
        columns={columns}
        data={tableData || []}
        editingMode="modal" //default
        enableEditing
        onEditingRowSave={handleSaveRowEdits}
        onEditingRowCancel={handleCancelRowEdits}
        renderRowActions={({ row, table }) => (
          <Box sx={{ display: 'flex', gap: '1rem' }}>
            <Tooltip arrow title="Edit">
              <IconButton onClick={() => table.setEditingRow(row)}>
                <Edit />
              </IconButton>
            </Tooltip>
            <Tooltip arrow title="View">
              <IconButton
                onClick={() => setSelectedTranslationId(row.getValue('id'))}
              >
                <Visibility />
              </IconButton>
            </Tooltip>
            <Tooltip arrow title="Delete">
              <IconButton color="error" onClick={() => handleDeleteRow(row)}>
                <Delete />
              </IconButton>
            </Tooltip>
          </Box>
        )}
        renderTopToolbarCustomActions={() => (
          <Button
            color="secondary"
            onClick={() => setCreateModalOpen(true)}
            variant="contained"
          >
            Create New Translation
          </Button>
        )}
      />
      <CreateNewTranslationModal
        columns={columns.filter((col) => col.enableEditing === undefined)}
        open={createModalOpen}
        onClose={() => setCreateModalOpen(false)}
      />
      <ViewTranslationModal
        columns={columns}
        selectedTranslationId={selectedTranslationId}
        onClose={() => setSelectedTranslationId(null)}
      />
    </>
  );
};

const validateRequired = (value: string) => !!value.length;

export default TranslationComponent;
