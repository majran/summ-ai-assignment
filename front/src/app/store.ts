import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { translationApi } from 'services/translation';
import logger from 'redux-logger';

export const store = configureStore({
  reducer: {
    
    // Add the generated reducer as a specific top-level slice
    [translationApi.reducerPath]: translationApi.reducer,
  },
  // Adding the api middleware enables caching, invalidation, polling,
  // and other useful features of `rtk-query`.
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(translationApi.middleware, logger),
  devTools: process.env.NODE_ENV !== 'production',
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
