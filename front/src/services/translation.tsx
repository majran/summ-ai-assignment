// Need to use the React-specific entry point to import createApi
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import type { Translation as TranslationType } from 'types';

const baseUrl: string = process.env.REACT_APP_API_ROOT as string;

// Define a service using a base URL and expected endpoints
export const translationApi = createApi({
  reducerPath: 'translationApi',
  baseQuery: fetchBaseQuery({ baseUrl }),
  tagTypes: ['Translations'],
  endpoints: (builder) => ({
    getTranslationList: builder.query<TranslationType[], void>({
      query: () => `translation/`,
      providesTags: (result) =>
        result
          ? [
              ...result.map(({ id }) => ({
                type: 'Translations' as const,
                id,
              })),
              { type: 'Translations', id: 'LIST' },
            ]
          : [{ type: 'Translations', id: 'LIST' }],
    }),
    getTranslationById: builder.query<TranslationType, string>({
      query: (id) => `translation/${id}/`,
      providesTags: (result, error, id) => [{ type: 'Translations', id }],
    }),
    addTranslation: builder.mutation<
      Partial<TranslationType>,
      Partial<TranslationType>
    >({
      query(body) {
        return {
          url: `translation/`,
          method: 'POST',
          body,
        };
      },
      invalidatesTags: [{ type: 'Translations', id: 'LIST' }],
    }),
    updateTranslation: builder.mutation<
      Partial<TranslationType>,
      { id: string; body: Partial<TranslationType> }
    >({
      query({ id, body }) {
        return {
          url: `/translation/${id}/`,
          method: 'PUT',
          body,
        };
      },
      invalidatesTags: (result, error, { id }) =>
        result
          ? [
              { type: 'Translations', id },
              { type: 'Translations', id: 'LIST' },
            ]
          : [{ type: 'Translations', id: 'LIST' }],
    }),
    deleteTranslation: builder.mutation<void, string>({
      query(id) {
        return {
          url: `/translation/${id}/`,
          method: 'Delete',
        };
      },
      invalidatesTags: [{ type: 'Translations', id: 'LIST' }],
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
  useGetTranslationListQuery,
  useGetTranslationByIdQuery,
  useAddTranslationMutation,
  useUpdateTranslationMutation,
  useDeleteTranslationMutation,
} = translationApi;
